-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 26 Agu 2021 pada 16.01
-- Versi server: 5.7.24
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `companies`
--

INSERT INTO `companies` (`id`, `nama`, `email`, `logo`, `website`, `created_at`, `updated_at`) VALUES
(1, 'Roni Setia1', 'ron@gmail.com', '1629961352.png', 'Transisi', '2021-08-25 23:55:55', '2021-08-26 00:02:32'),
(2, 'Cindy V', 'Transisi', '1629963291.png', 'cindy.co.id', '2021-08-26 00:34:51', '2021-08-26 00:34:51');

-- --------------------------------------------------------

--
-- Struktur dari tabel `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `employees`
--

INSERT INTO `employees` (`id`, `nama`, `company`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Roni', 'Transisi', 'roni1@gmail.com', '2021-08-26 00:31:00', '2021-08-26 00:31:00'),
(2, 'Cindy V', 'Transisi', 'cindi@gmail.com', '2021-08-26 00:31:21', '2021-08-26 00:32:38'),
(3, 'Bisri', 'Transisi', 'bisri@gmail.com', '2021-08-26 00:33:25', '2021-08-26 00:33:25'),
(4, 'Aditya', 'Primakom', 'adit@gmail.com', '2021-08-26 00:33:53', '2021-08-26 00:33:53'),
(5, 'Dicky', 'Transisi', 'dicky@gmail.com', '2021-08-26 00:34:10', '2021-08-26 00:34:10'),
(6, 'Juna', 'Transisi', 'juna@gmail.com', '2021-08-26 00:50:28', '2021-08-26 00:50:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_08_26_044228_create_companies_table', 2),
(6, '2021_08_26_044241_create_employees_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Roni Setiawan', 'ronitransisi@gmail.com', NULL, '$2y$10$oAoEpS5QuDZNnpvx7A4eru8byP/ZzbFr5xp3TthwCI27wJrkgYu42', NULL, '2021-08-25 20:35:29', '2021-08-25 20:35:29'),
(2, 'Nasir Jakubowski Sr.', 'sophie13@example.com', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'siHjod1uFZ', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(3, 'Madonna Kub DDS', 'melvin.fay@example.org', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jIAr8ujjZt', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(4, 'Kurtis Aufderhar', 'hconsidine@example.org', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Mw46illRL5', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(5, 'Nicolette Gusikowski IV', 'mertie85@example.net', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'V5vMWJPB2F', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(6, 'Desiree Welch', 'jdeckow@example.org', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DQTq2MGO2r', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(7, 'Modesta Kertzmann', 'vbruen@example.com', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ZFcPBw80sf', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(8, 'Wava Muller', 'gerhold.camylle@example.org', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '9vBhitAKeF', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(9, 'Prof. Marlee Kozey', 'brando.dietrich@example.com', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4Rua0Sr9By', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(10, 'Alberta Bins', 'alexa.boyer@example.com', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'U66EldMViN', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(11, 'Mr. Seamus Jerde', 'shad.zulauf@example.net', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'vavjqgpzPc', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(12, 'Dr. Narciso Stanton', 'ukuphal@example.com', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IKXAeaqgA1', '2021-08-26 07:50:03', '2021-08-26 07:50:03'),
(13, 'Kaley Halvorson DDS', 'aditya.kassulke@example.net', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'leXGOO9xcn', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(14, 'Piper Wyman', 'piper12@example.org', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XR5BaQVvVP', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(15, 'Roel Toy DVM', 'davis.milton@example.org', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'x8z3itHtAQ', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(16, 'Bernadette Fahey', 'eric.aufderhar@example.net', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1hkiiS5C8R', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(17, 'Marcos Paucek', 'hermann.kelvin@example.org', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UpHjSYb5yp', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(18, 'Prof. Dagmar Mueller', 'velva.lebsack@example.com', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CyTunTYBiv', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(19, 'Miss Breanne Moore', 'yblanda@example.net', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ZfDa28fgoQ', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(20, 'Rebekah Greenfelder', 'dtreutel@example.com', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UP07I0FtL4', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(21, 'Mr. Benton Jacobson DDS', 'eichmann.dax@example.org', '2021-08-26 07:50:03', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KKsGsd2s3N', '2021-08-26 07:50:04', '2021-08-26 07:50:04'),
(26, '5', 'Nicolette Gusikowski IV', NULL, '$2y$10$ypeGcDkcBA8p3MNk0lDQNeDrL/a/Q7uKqzPDrtf.OS/3zFhrBwkqe', NULL, '2021-08-26 08:56:54', '2021-08-26 08:56:54'),
(27, '6', 'Desiree Welch', NULL, '$2y$10$yv9eE.WUHiao99Lk.H5Cve1l6QaWundAfkLgJ2SdCd7P0vpYAr4Bu', NULL, '2021-08-26 08:56:54', '2021-08-26 08:56:54'),
(28, '7', 'Modesta Kertzmann', NULL, '$2y$10$sm1j.u4cB4eLvGjHJVeug.zAV4PcembzTvSJZHFUpb6EF.Az4Ws1G', NULL, '2021-08-26 08:56:55', '2021-08-26 08:56:55'),
(29, '8', 'Wava Muller', NULL, '$2y$10$CArhCWjUwz7O373mAfNcneXSWGQlEa1RwhAblrhiCCf2BqOevdGp6', NULL, '2021-08-26 08:56:55', '2021-08-26 08:56:55'),
(30, '9', 'Prof. Marlee Kozey', NULL, '$2y$10$AD/JN.U95eMDXTYsVuVmq.D/XkVlkrk/1HMi.1tJcwVT0x972vX/W', NULL, '2021-08-26 08:56:55', '2021-08-26 08:56:55'),
(31, '10', 'Alberta Bins', NULL, '$2y$10$fwM4NAgBtVF/s/tGKttiaONszm47Dz/8YApJnWyPzTSuF0vguxotm', NULL, '2021-08-26 08:56:55', '2021-08-26 08:56:55'),
(32, '11', 'Mr. Seamus Jerde', NULL, '$2y$10$J1z/7Fm78Jf4Nbnx20B/XeQM4dHr.VHcVHgXac6FFiDhcYKIwu7Ou', NULL, '2021-08-26 08:56:55', '2021-08-26 08:56:55'),
(33, '12', 'Dr. Narciso Stanton', NULL, '$2y$10$hiwguzifHBEBJTVHiWmAfed/toPy5OvHpOEEnFXj0SEZ.IHW6Gd.u', NULL, '2021-08-26 08:56:55', '2021-08-26 08:56:55'),
(34, '13', 'Kaley Halvorson DDS', NULL, '$2y$10$8SzcLaoYH8rpz62FgQJ3EOmt/Jboi6i2F3lNuHQCe/VJyPmePGqpe', NULL, '2021-08-26 08:56:55', '2021-08-26 08:56:55'),
(35, '14', 'Piper Wyman', NULL, '$2y$10$yG/TICElPzMxMHgjxDxnvuQwh9gASdouHdNx0zN6m85VU0oGigI0G', NULL, '2021-08-26 08:56:56', '2021-08-26 08:56:56'),
(36, '15', 'Roel Toy DVM', NULL, '$2y$10$JuqHV2Qu0SaxW0mKsY41Mu/LUCME0Y7tNDUCdAnstpJDGbANhJ5xq', NULL, '2021-08-26 08:56:56', '2021-08-26 08:56:56'),
(37, '16', 'Bernadette Fahey', NULL, '$2y$10$t5Yx5szvBFSopqMfrlG1Q.hhs4ZHi4h02Y5kCIx/M9mhrESYT2xsm', NULL, '2021-08-26 08:56:56', '2021-08-26 08:56:56'),
(38, '17', 'Marcos Paucek', NULL, '$2y$10$oh9SYfEK92h0i1NJIOcTFOYiNiES4OkzKfMk7Rcij6KnPV7rzBrlC', NULL, '2021-08-26 08:56:56', '2021-08-26 08:56:56'),
(39, '18', 'Prof. Dagmar Mueller', NULL, '$2y$10$21Tclb8KaySfiTW1J0JA0eCgcPGXh5KJf9WAXXU3CLiNIhpatte4C', NULL, '2021-08-26 08:56:56', '2021-08-26 08:56:56'),
(40, '19', 'Miss Breanne Moore', NULL, '$2y$10$IHeAg6UlAXGdBPUh778WVek/ssy6nTYBhjZHxiVQmtDckCXIiF/oC', NULL, '2021-08-26 08:56:56', '2021-08-26 08:56:56'),
(41, '20', 'Rebekah Greenfelder', NULL, '$2y$10$YdtTUlgDgoYHlY1p1V2ghuZVbxO80z8ipIqrFjLm1lHjX3NWM1nKe', NULL, '2021-08-26 08:56:56', '2021-08-26 08:56:56'),
(42, '21', 'Mr. Benton Jacobson DDS', NULL, '$2y$10$xlHOv5vC2BUazD7NlOwenOplxPWqrALImQhmyScMLHm3WaTQ3P2O2', NULL, '2021-08-26 08:56:57', '2021-08-26 08:56:57');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company` (`company`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
