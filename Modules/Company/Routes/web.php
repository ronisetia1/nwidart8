<?php

use Modules\Company\Http\Controllers\CompanyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('company')->group(function() {
    Route::get('/', 'CompanyController@index');
});


// Company
// Route::get('companies', [CompanyController::class, 'index']);
Route::get('add-companies', [CompanyController::class, 'create']);
Route::post('add-companies', [CompanyController::class, 'store']);
Route::get('edit-companies/{id}', [CompanyController::class, 'edit']);
Route::put('update-companies/{id}', [CompanyController::class, 'update']);
Route::delete('delete-companies/{id}', [CompanyController::class, 'destroy']);