@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">

            @if (session('status'))
                <h6 class="alert alert-success">{{ session('status') }}</h6>
            @endif

            <div class="card">
                <div class="card-header">
                    <h4>Edit Company
                        <a href="{{ url('companies') }}" class="btn btn-danger float-end">BACK</a>
                    </h4>
                </div>
                <div class="card-body">

                     <form action="{{ url('update-companies/'.$companies->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group mb-3">
                            <label for="">Nama</label>
                            <input type="text" name="nama" value="{{$companies->nama}}" class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Email</label>
                            <input type="text" name="email" value="{{$companies->email}}" class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Website</label>
                            <input type="text" name="website" value="{{$companies->website}}" class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Logo Company</label>
                            <input type="file" name="logo" class="form-control">
                            <img src="{{ asset('uploads/companies/'.$companies->logo) }}" width="70px" height="70px" alt="Image">
                        </div>
                        <div class="form-group mb-3">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection