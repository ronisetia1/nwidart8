<?php

namespace Modules\Company\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Company\Entities\Company;
use File;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $companies = Company::latest()->paginate(5);

        return view('company::index',compact('companies'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('company::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $companies = new Company;
        $companies->nama = $request->input('nama');
        $companies->email = $request->input('email');
        $companies->website = $request->input('website');
        if($request->hasfile('logo'))
        {
            $file = $request->file('logo');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('uploads/companies/', $filename);
            $companies->logo = $filename;
        }
        $companies->save();
        return redirect()->back()->with('status','Company Added Successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('company::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $companies = Company::find($id);
        return view('company::edit', compact('companies'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $companies = Company::find($id);
        $companies->nama = $request->input('nama');
        $companies->email = $request->input('email');
        $companies->website = $request->input('website');

        if($request->hasfile('logo'))
        {
            $destination = 'uploads/companies/'.$companies->logo;
            if(File::exists($destination))
            {
                File::delete($destination);
            }
            $file = $request->file('logo');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('uploads/companies/', $filename);
            $companies->logo = $filename;
        }

        $companies->update();
        return redirect()->back()->with('status','Company Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $companies = Company::find($id);
        $destination = 'uploads/companies/'.$companies->logo;
        if(File::exists($destination))
        {
            File::delete($destination);
        }
        $companies->delete();
        return redirect()->back()->with('status','Company Deleted Successfully');
    }
}
