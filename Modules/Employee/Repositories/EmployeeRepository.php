<?php

namespace Modules\Employee\Repositories;

use Modules\Employee\Repositories\Employee\EmployeeInterface as EmployeeInterface;

class CompanyRepository {
  
  	protected $employees;

	public function __construct(Employee $employees)
	{
        $this->employees = $employees;
    }
    public function findById($id)
    {
        return $this->employees->find($id);
    }
    public function getAllPagination($page)
    {
        return $this->employees->paginate($page);
    }
}

?>