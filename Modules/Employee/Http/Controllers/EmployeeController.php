<?php

namespace Modules\Employee\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Employee\Entities\Employee;
use PDF;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {

        $employees = Employee::orderBy('id','DESC')->paginate(5);

        return view('employee::index',compact('employees'))
                    ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('employee::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $employees = new Employee;
        $employees->nama = $request->input('nama');
        $employees->company = $request->input('company');
        $employees->email = $request->input('email');
       
        $employees->save();
        return redirect()->back()->with('status','Employee Added Successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('employee::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $employees = Employee::find($id);

        return view('employee::edit', compact('employees'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $employees = Employee::find($id);
        $employees->nama = $request->input('nama');
        $employees->company = $request->input('company');
        $employees->email = $request->input('email');

        $employees->update();
        return redirect()->back()->with('status','Employee Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $employees = Employee::find($id);
        
        $employees->delete();
        return redirect()->back()->with('status','Employee Deleted Successfully');
    }

    public function cetak_pdf()
    {
    	$employees = Employee::all();
 
    	$pdf = PDF::loadview('employee::employee_pdf',['employees'=>$employees]);
    	return $pdf->download('laporan-employee-pdf');
    }
}
