<?php

use Modules\Employee\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('employee')->group(function() {
    Route::get('/', 'EmployeeController@index');
});

Route::get('add-employees', [EmployeeController::class, 'create']);
Route::post('add-employees', [EmployeeController::class, 'store']);
Route::get('edit-employees/{id}', [EmployeeController::class, 'edit']);
Route::put('update-employees/{id}', [EmployeeController::class, 'update']);
Route::delete('delete-employees/{id}', [EmployeeController::class, 'destroy']);

// PDF
Route::get('/employee/cetak_pdf', 'EmployeeController@cetak_pdf');
