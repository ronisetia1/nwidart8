@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Laravel 8 CRUD
                        <a href="{{ url('add-employees') }}" class="btn btn-primary float-end">Add Employee</a>
                        <a href="/employee/cetak_pdf" class="btn btn-primary" target="_blank">CETAK PDF</a>
                    </h4>
                </div>
                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Company</th>
                                <th>Email</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($employees as $item)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->company }}</td>
                                <td>{{ $item->email }}</td>
                                <td>
                                    <a href="{{ url('edit-employees/'.$item->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                </td>
                                <td>
                                    {{-- <a href="{{ url('delete-student/'.$item->id) }}" class="btn btn-danger btn-sm">Delete</a> --}}
                                    <form action="{{ url('delete-employees/'.$item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <br/>
                    Halaman : {{ $employees->currentPage() }}<br/>
                    Jumlah Data : {{ $employees->total() }}<br/>
                    Data Per Halaman : {{ $employees->perPage() }}
                    <br/>

                    <br/>

                    {{ $employees->links() }}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection